// Neopixels
#include <Adafruit_NeoPixel.h>
#define NeoPIN 6
#define NUM_LEDS 3
int brightness = 150;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_RGB + NEO_KHZ800);
uint32_t shellColors [NUM_LEDS][3];

#define TriggerPin 2
unsigned long aniStart = 0L;
unsigned long aniTime = 1000L * 20L; // 20 seconds
unsigned long aniTimeShell = (aniTime / NUM_LEDS); // animation time for each shell
byte shellPos = 0;
bool isTriggered = false;
int colorSteps [NUM_LEDS];

unsigned long stepper = millis();
int currentLed = 0;
int rgb = 0;

void setup() {
  // Serial
  // serial output
  Serial.begin(9600);
  Serial.println("====================");
  Serial.println("    3 Seashells");
  Serial.println("(c)2020 Agis Wichert");
  Serial.println("====================");
  delay(500);
  
  // random
  randomSeed(analogRead(A0));

  // Neopixel
  strip.setBrightness(brightness);
  strip.begin();
  strip.show();
 
}

void loop() {
  if(isTriggered == false){
    // check for distance sensor
    int trigger = digitalRead(TriggerPin);
    if(trigger == LOW){
      isTriggered = true;
      // assigning three random colors
      for(int i = 0; i < NUM_LEDS; i++){
        colorSteps[i] = 0;
        for(int t = 0; t < 3; t++){
          shellColors[i][t] = random(20,255);
          colorSteps[i] += (int)shellColors[i][t];
        }
        colorSteps[i] = colorSteps[i] - 10;
        Serial.print("CS: ");
        Serial.print(colorSteps[i]);
        Serial.print(" - ");
        colorSteps[i] = aniTimeShell / colorSteps[i];
        strip.setPixelColor(i, strip.Color(shellColors[i][0],shellColors[i][1],shellColors[i][2]));
        Serial.println(colorSteps[i]);
      }
      strip.show();
      aniStart = millis();
      stepper = millis();
      currentLed = 0;
      rgb = 0;
    }
  }
  else{
    // still within the animation time
    if((aniStart + aniTime) > millis()){

       // still within the animation time of the current led
       unsigned long r = aniStart + (aniTimeShell * (currentLed + 1));
      if( r < millis() ){
        rgb = 0;
        strip.setPixelColor(currentLed++, strip.Color(0,0,0));
        Serial.print("Changing LED to: ");
        Serial.println(currentLed, DEC);
        Serial.print("At:" );
        Serial.println( r, DEC);
      }
      
      // still within the animation time of the current led
      if((stepper + colorSteps[currentLed] ) < millis()){

        /*
        Serial.print("Steps ");
        Serial.print(stepper, DEC);
        Serial.print(" . ");
        Serial.println(colorSteps[currentLed], DEC);
        */
        
        if(shellColors[currentLed][rgb] > 0){
          // changing color
          shellColors[currentLed][rgb]--;
          strip.setPixelColor(currentLed, strip.Color(shellColors[currentLed][0],shellColors[currentLed][1],shellColors[currentLed][2]));
          strip.show();
        }
        else{
          if(rgb >= 3){
            // current led is dark
            //rgb = 0;
            //currentLed++;
            //Serial.print("Changing LED to: ");
            //Serial.println(currentLed, DEC);
          }
          else{
            // current color is dark
            Serial.print("RGB : ");
            Serial.println(rgb, DEC);
            rgb++;
          }
        }
        stepper = millis();
      }
      
      
    }
    else{
      // finished
      strip.fill(strip.Color(0,0,0), 0,NUM_LEDS);
      strip.show();
      Serial.println("Off");
      for(int i = 0; i < 3; i++){
        strip.fill(strip.Color(200,0,0), 0,NUM_LEDS);
        strip.show();
        delay(100);
        strip.fill(strip.Color(0,0,0), 0,NUM_LEDS);
        strip.show();
        delay(100);
      }
      isTriggered = false;
      rgb = 0;
      currentLed = 0;
    }
  }
  
  
}
